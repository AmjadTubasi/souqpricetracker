//
//  AppUtility.h
//  BeginHiring
//
//  Created by Amjad Mac on 12/19/15.
//  Copyright © 2015 Amjad Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface AppUtility : NSObject
+(NSString *)getProductTypesIds:(NSMutableArray *)selectedProductTypes;

@end
