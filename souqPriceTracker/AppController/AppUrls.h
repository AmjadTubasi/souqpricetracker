//
//  AppUrls.h
//  souqPriceTracker
//
//  Created by Amjad Mac on 3/5/16.
//  Copyright © 2016 souq. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppUrls : NSObject
+(NSString *)getProductTypesUrlWithPageNumber:(int)pageNumber;
+(NSString *)getProductsUrlWithPageNumber:(int)pageNumber productTypes:(NSMutableArray *)selectedProductTypes searchText:(NSString *)searchText;
@end
