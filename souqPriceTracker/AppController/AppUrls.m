//
//  AppUrls.m
//  souqPriceTracker
//
//  Created by Amjad Mac on 3/5/16.
//  Copyright © 2016 souq. All rights reserved.
//

#import "AppUrls.h"

@implementation AppUrls

+(NSString *)getProductTypesUrlWithPageNumber:(int)pageNumber{

    
    
    return [NSString stringWithFormat:@"products/types?page=%d&show=%@&language=en&format=json&app_id=%@&app_secret=%@",pageNumber,API_PARAM_PAGE_SIZE,APP_ID,APP_SECRET];
    
}


+(NSString *)getProductsUrlWithPageNumber:(int)pageNumber productTypes:(NSMutableArray *)selectedProductTypes searchText:(NSString *)searchText{
    
    
    
    
    
    return [NSString stringWithFormat:@"products?q=%@&product_types=%@&page=%d&show=%@&show_attributes=1&country=ae&language=en&format=json&app_id=%@&app_secret=%@",searchText,[AppUtility getProductTypesIds:selectedProductTypes],pageNumber,API_PARAM_PAGE_SIZE,APP_ID,APP_SECRET];
    
}
@end
