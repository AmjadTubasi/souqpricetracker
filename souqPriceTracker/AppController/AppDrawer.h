//
//  AppDrawer.h
//  souqPriceTracker
//
//  Created by Amjad Mac on 3/6/16.
//  Copyright © 2016 souq. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppDrawer : NSObject<UIAlertViewDelegate>
+(BOOL)isLoadMore:(UIScrollView *)scrollView;
+(void)showAlertWithTitle:(NSString *)title withMessage:(NSString *)message withButton:(NSString *)button;
@end
