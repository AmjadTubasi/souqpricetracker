//
//  DatabaseHelper.h
//
//  Created by Amjad Tubasi on 6/28/13.
//  Copyright (c) 2013 Amjad Tubasi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FMDatabase.h>

@interface DatabaseHelper : FMDatabase
-(void)execQuery:(NSString*)query;
+(id)sharedInstance;


-(void)insertOrDeleteProductFromFavorites:(ProductObject *)product;
-(NSMutableArray *)getFavoritesProducts;
-(BOOL)isProductInFavorites:(NSString *)productId;
@end