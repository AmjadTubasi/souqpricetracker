//
//  AppUtility.m
//  BeginHiring
//
//  Created by Amjad Mac on 12/19/15.
//  Copyright © 2015 Amjad Mac. All rights reserved.
//

#import "AppUtility.h"

@implementation AppUtility
+(NSString *)getProductTypesIds:(NSMutableArray *)selectedProductTypes{
    
    NSMutableArray *strings = [NSMutableArray new];
    
    for(ProductTypeObject *productType in selectedProductTypes) {
        
        
        [strings addObject:productType.Id];
    }
    
    
    return [strings componentsJoinedByString:@","];
    
}
@end
