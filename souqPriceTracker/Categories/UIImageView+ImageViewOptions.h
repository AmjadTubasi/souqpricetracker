//
//  UIImageView+ImageViewOptions.h
//  souqPriceTracker
//
//  Created by Amjad Mac on 3/5/16.
//  Copyright © 2016 souq. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (ImageViewOptions)
-(void)imageWithUrl:(NSString *)imageUrl;

@end
