//
//  UITextField+TextFieldOptions.h
//  souqPriceTracker
//
//  Created by Amjad Mac on 3/5/16.
//  Copyright © 2016 souq. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (TextFieldOptions)
@property (nonatomic) IBInspectable UIImage *leftViewImage;
@property (nonatomic) IBInspectable UIColor *placeholderColor;

@end
