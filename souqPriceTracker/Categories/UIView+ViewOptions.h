//
//  UIView+ViewOptions.h
//  souqPriceTracker
//
//  Created by Amjad Mac on 3/5/16.
//  Copyright © 2016 souq. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (ViewOptions)
@property (nonatomic) IBInspectable UIColor *borderColor;
@property (nonatomic) IBInspectable NSInteger borderWidth;
@property (nonatomic) IBInspectable NSInteger cornerRadius;
@property (nonatomic) IBInspectable UIRectCorner rectCorners;
@end
