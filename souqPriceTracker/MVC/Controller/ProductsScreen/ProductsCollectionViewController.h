//
//  ProductsCollectionViewController.h
//  souqPriceTracker
//
//  Created by Amjad Mac on 3/6/16.
//  Copyright © 2016 souq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductCollectionViewCell.h"
@interface ProductsCollectionViewController : UICollectionViewController<ProductFavoritesDelegate>{
    
    NSMutableArray *AllData;
    
    int pageNumber;
}
@property (nonatomic)NSMutableArray *selectedProductTypes;

@end
