//
//  ProductsCollectionViewController.m
//  souqPriceTracker
//
//  Created by Amjad Mac on 3/6/16.
//  Copyright © 2016 souq. All rights reserved.
//

#import "ProductsCollectionViewController.h"
#import "OfferDetailsTableViewController.h"

@interface ProductsCollectionViewController ()

@end

@implementation ProductsCollectionViewController

static NSString * const reuseIdentifier = @"ProductCollectionViewCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Register cell classes
//    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifier];
    
    
    AllData = [NSMutableArray new];
    pageNumber = 1;
    [self getProducts:YES];
    // Do any additional setup after loading the view.
    

}
-(void)getProducts:(BOOL)withProgress{
    
    [AppWrapper getProductsWithPageNumber:1 selectedProductTypes:self.selectedProductTypes searchText:@"" withProgress:withProgress completion:^(NSMutableArray *data,int total){
        
        
        
        AllData = data;
        
        [self.collectionView reloadData];
        
        
        
    }];
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return AllData.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ProductCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    // Configure the cell
    
    cell.productObject = AllData[indexPath.row];
    cell.delegate =self;
    
    return cell;
    
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSLog(@"SETTING SIZE FOR ITEM AT INDEX %f", self.view.frame.size.width/2);
    CGSize mElementSize = CGSizeMake((self.view.frame.size.width/2)-10, (self.view.frame.size.width/2)-10);
    return mElementSize;
}

-(void)productFavoriteButtonPressed:(ProductObject *)productObject{
    
    

    [[DatabaseHelper sharedInstance]insertOrDeleteProductFromFavorites:productObject];
    
    productObject.isFavorites = !productObject.isFavorites;
    
    
    
    [self.collectionView reloadData];
}
#pragma mark <UICollectionViewDelegate>

/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/

/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    OfferDetailsTableViewController *con = getViewController(@"OfferDetailsTableViewController");
    con.productObject = AllData[indexPath.row];
    
    [self.navigationController pushViewController:con animated:YES];
}

- (IBAction)addOrRemoveFavorites:(id)sender {
}
@end
