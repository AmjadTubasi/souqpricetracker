//
//  FavoritesCollectionViewController.h
//  souqPriceTracker
//
//  Created by Amjad Mac on 3/6/16.
//  Copyright © 2016 souq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductCollectionViewCell.h"

@interface FavoritesCollectionViewController : UICollectionViewController<ProductFavoritesDelegate>{
    
    NSMutableArray *AllData;

}

@end
