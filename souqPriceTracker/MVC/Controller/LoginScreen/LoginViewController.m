//
//  LoginViewController.m
//  souqPriceTracker
//
//  Created by Amjad Mac on 3/5/16.
//  Copyright © 2016 souq. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)loginUser:(id)sender {
    
    
    if (![self isValidToLogin])
        return;
    
    
    
    
    
    [[AppDelegate appDelegate]changeRootViewController:[[UINavigationController alloc]initWithRootViewController:getViewController(@"ProductTypesViewController")]];
    
    
}

-(BOOL)isValidToLogin{
    
    
    if (![self.fieldUserName validate]){
        
        return NO;
    }else if (![self.fieldPassword validate]){
        return NO;

    }
    
    return YES;
}
@end
