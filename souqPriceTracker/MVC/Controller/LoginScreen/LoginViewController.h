//
//  LoginViewController.h
//  souqPriceTracker
//
//  Created by Amjad Mac on 3/5/16.
//  Copyright © 2016 souq. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController
@property (weak, nonatomic) IBOutlet TextFieldValidator *fieldPassword;

- (IBAction)loginUser:(id)sender;
@property (weak, nonatomic) IBOutlet TextFieldValidator *fieldUserName;
@property (weak, nonatomic) IBOutlet UIButton *buttonLogin;
@end
