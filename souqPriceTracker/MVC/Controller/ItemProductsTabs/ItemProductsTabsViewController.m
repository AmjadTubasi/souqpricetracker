//
//  ItemProductsTabsViewController.m
//  souqPriceTracker
//
//  Created by Amjad Mac on 3/6/16.
//  Copyright © 2016 souq. All rights reserved.
//

#import "ItemProductsTabsViewController.h"
#import "ProductsCollectionViewController.h"
@interface ItemProductsTabsViewController ()

@end

@implementation ItemProductsTabsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = NSLocalizedString(@"TITLE_TABS", nil);
    
    ProductsCollectionViewController *controller = self.viewControllers[0];
    
    controller.selectedProductTypes = self.selectedProductTypes;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//#pragma mark - Navigation
//
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    // Get the new view controller using [segue destinationViewController].
//    // Pass the selected object to the new view controller.
//    
//    if ([segue.identifier isEqualToString:@"tab1"]){
//        
//        ProductsCollectionViewController *controller = segue.destinationViewController;
//        controller.selectedProductTypes = _selectedProductTypes;
//    }
//    
//    
//}


@end
