//
//  ItemProductsTabsViewController.h
//  souqPriceTracker
//
//  Created by Amjad Mac on 3/6/16.
//  Copyright © 2016 souq. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItemProductsTabsViewController : UITabBarController
@property (nonatomic)NSMutableArray *selectedProductTypes;
@end
