//
//  OfferDetailsTableViewController.h
//  souqPriceTracker
//
//  Created by Amjad Mac on 3/6/16.
//  Copyright © 2016 souq. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OfferDetailsTableViewController : UITableViewController
@property (weak, nonatomic) IBOutlet UILabel *labelProductName;
@property (weak, nonatomic) IBOutlet UILabel *labelProductPrice;
@property (weak, nonatomic) IBOutlet UIButton *buttonFavorites;
- (IBAction)addToFavorites:(id)sender;

@property (weak, nonatomic) IBOutlet UIImageView *imageProduct;


@property (nonatomic)ProductObject *productObject;
@end
