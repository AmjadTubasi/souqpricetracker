//
//  OfferDetailsTableViewController.m
//  souqPriceTracker
//
//  Created by Amjad Mac on 3/6/16.
//  Copyright © 2016 souq. All rights reserved.
//

#import "OfferDetailsTableViewController.h"

@interface OfferDetailsTableViewController ()

@end

@implementation OfferDetailsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    
    self.labelProductName.text = _productObject.name;
    [self.imageProduct imageWithUrl:_productObject.imageXL.imageUrl];
    self.labelProductPrice.text = [NSString stringWithFormat:@"%@ %@",_productObject.offerPrice,_productObject.currency];
    
    [self setFavoritsImage];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setFavoritsImage{
    
    if(_productObject.isFavorites){
        
        [self.buttonFavorites setImage:[UIImage imageNamed:@"added_to_favorites"] forState:UIControlStateNormal];
    }else{
        
        [self.buttonFavorites setImage:[UIImage imageNamed:@"add_to_favorites"] forState:UIControlStateNormal];
        
        
    }
}
- (IBAction)addToFavorites:(id)sender {
    
    [[DatabaseHelper sharedInstance]insertOrDeleteProductFromFavorites:_productObject];

    
    _productObject.isFavorites =! _productObject.isFavorites;
    
  
    [self setFavoritsImage];
    
    
}
@end
