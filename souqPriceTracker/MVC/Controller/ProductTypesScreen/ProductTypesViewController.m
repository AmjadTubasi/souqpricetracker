//
//  ProductTypesViewController.m
//  souqPriceTracker
//
//  Created by Amjad Mac on 3/5/16.
//  Copyright © 2016 souq. All rights reserved.
//

#import "ProductTypesViewController.h"
#import "ItemProductsTabsViewController.h"

@interface ProductTypesViewController ()

@end

@implementation ProductTypesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    AllProductTypes = [NSMutableArray new];
    totalTypes = 0;
    pageNumber = 1;
    
    refreshControl = [UIRefreshControl new];
    refreshControl.triggerVerticalOffset = 100.;
    [refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    self.tableView.bottomRefreshControl = refreshControl;
    [self getProductTypes:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)getProductTypes:(BOOL)withProgress{
    
    
    
   
        [AppWrapper getProductTypesWithPageNumber:pageNumber withProgress:withProgress  completion:^(NSMutableArray *data,int total){
     
            
            totalTypes = total;
            
            [AllProductTypes addObjectsFromArray:data];
            [self.tableView reloadData];
            [refreshControl endRefreshing];

            
        }];
    
    
    
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    
    return AllProductTypes.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    ProductTypeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProductTypeCell"];
    
    if (!cell)
        cell = [[ProductTypeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ProductTypeCell"];
    
    
    cell.productTypeObject = AllProductTypes[indexPath.row];
    
    
    return cell;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    ProductTypeObject *productType =  AllProductTypes[indexPath.row];
    
    productType.isSelected = !productType.isSelected;
    
    [self.tableView reloadData];
}
//- (void)scrollViewDidScroll:(UIScrollView *)aScrollView {
//
//
//    if (AllProductTypes.count!=totalTypes &&  [AppDrawer isLoadMore:aScrollView]){
//        
//        pageNumber+=1;
//
//   
//    }
//
//  
//    
//}


-(NSMutableArray *)filterSelectedProducts{
    
    
    
    
    NSMutableArray *selectedProductTypes = [NSMutableArray new];
    
    for (ProductTypeObject *productType in AllProductTypes){
        
        
        if (productType.isSelected)
            [selectedProductTypes addObject:productType];
        
    }
    
    
    return selectedProductTypes;
    
}
- (IBAction)saveSaveProductTypes:(id)sender {
    
    
    NSMutableArray *selectedProductTypes =[self filterSelectedProducts];
    
    if (selectedProductTypes.count>0){
        ItemProductsTabsViewController *tabs = getViewController(@"ItemProductsTabsViewController");
        tabs.selectedProductTypes = selectedProductTypes;
        [self.navigationController pushViewController:tabs animated:YES];
    }else{
        
        [AppDrawer showAlertWithTitle:NSLocalizedString(@"TITLE_NO_PRODUCTS", nil)   withMessage:NSLocalizedString(@"MSG_NO_PRODUCTS", nil)     withButton:NSLocalizedString(@"BUTTON_OKO", nil)];
    }
    
    
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (([scrollView contentOffset].y + scrollView.frame.size.height) >= [scrollView contentSize].height){
        
        // Get new record from here
        
      

    }
}
- (void)refresh {
    // Do refresh stuff here
    
    if (AllProductTypes.count!=totalTypes){
        
        pageNumber+=1;
        [self getProductTypes:NO];
        
        
    }else{
        
        [refreshControl endRefreshing];
    }
}

@end
