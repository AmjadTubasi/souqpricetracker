//
//  ProductTypesViewController.h
//  souqPriceTracker
//
//  Created by Amjad Mac on 3/5/16.
//  Copyright © 2016 souq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductTypeCell.h"
#import <CCBottomRefreshControl/UIScrollView+BottomRefreshControl.h>

@interface ProductTypesViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    
    
    NSMutableArray *AllProductTypes;
    UIRefreshControl *refreshControl;
    int pageNumber,totalTypes;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *buttonGo;



- (IBAction)saveSaveProductTypes:(id)sender;

@end
