//
//  ProductTypeObject.h
//  souqPriceTracker
//
//  Created by Amjad Mac on 3/5/16.
//  Copyright © 2016 souq. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProductTypeObject : NSObject


@property (nonatomic,strong)NSString *Id;
@property (nonatomic,strong)NSString *nameSingluar;
@property (nonatomic,strong)NSString *namePluar;
@property (nonatomic,strong)NSString *link;
@property (nonatomic,assign)BOOL isSelected;



-(instancetype)initWithDictionary:(NSDictionary *)dic;
@end
