//
//  ProductImageObject.h
//  souqPriceTracker
//
//  Created by Amjad Mac on 3/6/16.
//  Copyright © 2016 souq. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProductImageObject : NSObject
@property (nonatomic,strong)NSString *imageUrl;
@property (nonatomic,assign)ImageTypes imageType;

-(instancetype)initImageUrl:(NSString *)imageUrl  imageType:(ImageTypes)imageType;


@end
