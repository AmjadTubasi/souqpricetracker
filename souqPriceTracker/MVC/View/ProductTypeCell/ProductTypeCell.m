//
//  ProductTypeCell.m
//  souqPriceTracker
//
//  Created by Amjad Mac on 3/5/16.
//  Copyright © 2016 souq. All rights reserved.
//

#import "ProductTypeCell.h"

@implementation ProductTypeCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    // Configure the view for the selected state
}
-(void)setProductTypeObject:(ProductTypeObject *)productTypeObject{
    
    
    _productTypeObject = productTypeObject;
    self.labelProductTypeName.text =productTypeObject.nameSingluar;
    
    if (productTypeObject.isSelected){
        
        self.imageSelected.hidden = NO;
        self.viewContaniner.backgroundColor = BLUE_COLOR;
        self.labelProductTypeName.textColor = [UIColor whiteColor];
        
    }else{
        
        self.imageSelected.hidden = YES;
        self.viewContaniner.backgroundColor = GRAY_COLOR;
        self.labelProductTypeName.textColor = [UIColor blackColor];

        
    }
    
    
    
}
@end
