//
//  ProductTypeCell.h
//  souqPriceTracker
//
//  Created by Amjad Mac on 3/5/16.
//  Copyright © 2016 souq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductTypeObject.h"
@interface ProductTypeCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelProductTypeName;
@property (weak, nonatomic) IBOutlet UIImageView *imageSelected;
@property (weak, nonatomic) IBOutlet UIView *viewContaniner;

@property (nonatomic,copy)ProductTypeObject *productTypeObject;

@end
