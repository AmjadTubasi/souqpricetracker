//
//  ProductCollectionViewCell.m
//  souqPriceTracker
//
//  Created by Amjad Mac on 3/6/16.
//  Copyright © 2016 souq. All rights reserved.
//

#import "ProductCollectionViewCell.h"

@implementation ProductCollectionViewCell


-(void)setProductObject:(ProductObject *)productObject{
    
    
    _productObject = productObject;
    
    self.labelProductName.text =productObject.name;
    [self.imageProduct imageWithUrl:productObject.imageXL.imageUrl];

    
    
    if (productObject.isFavorites){
        
        [self.buttonFavorites setImage:[UIImage imageNamed:@"active_favorite"] forState:UIControlStateNormal];
    }else{
        [self.buttonFavorites setImage:[UIImage imageNamed:@"deactive_favorite"] forState:UIControlStateNormal];

    }
}


- (IBAction)addOrRemoveFavorites:(id)sender{
    
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(productFavoriteButtonPressed:)]) {
        [self.delegate productFavoriteButtonPressed:_productObject];
    }
    
}
@end
