//
//  ProductCollectionViewCell.h
//  souqPriceTracker
//
//  Created by Amjad Mac on 3/6/16.
//  Copyright © 2016 souq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductObject.h"



@protocol ProductFavoritesDelegate<NSObject>


@required
-(void)productFavoriteButtonPressed:(ProductObject *)productObject ;


@end
@interface ProductCollectionViewCell : UICollectionViewCell


@property (weak, nonatomic) IBOutlet UIImageView *imageProduct;
@property (weak, nonatomic) IBOutlet UILabel *labelProductName;
@property (weak, nonatomic) IBOutlet UIButton *buttonFavorites;

@property (nonatomic,copy)ProductObject *productObject;
- (IBAction)addOrRemoveFavorites:(id)sender;



@property (nonatomic, weak) id<ProductFavoritesDelegate> delegate;

@end
